import { promises as fs } from 'fs'
import getWabt from 'wabt'

export const watToWasm = async (inputWatFilePath, outputWasmFilePath) => {
  // sry Yoda
  try {
    const wabt = await getWabt()
    const file = await fs.readFile(inputWatFilePath, 'utf8')

    const wasmModule   = wabt.parseWat(inputWatFilePath, file)
    const moduleBinary = wasmModule.toBinary({})
    const moduleBuffer = Buffer.from(moduleBinary.buffer)

    await fs.writeFile(outputWasmFilePath, moduleBuffer)
  } catch (error) {
    console.error('build.js | WatToWasm | It\'s a trap!\n', error)
  }
}

export default { watToWasm }
