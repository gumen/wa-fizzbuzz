import assert from 'assert'
import build  from './build.js'
import wasm   from './wasm.js'

import fizzBuzzMap from './public/fizzbuzz-map.js'
import memoryParser from './public/memory-parser.js'

const watFilePath  = './fizzbuzz.wat'
const wasmFilePath = './fizzbuzz.wasm'

beforeEach(async () => {
  await build.watToWasm(watFilePath, wasmFilePath)
})

describe('fizzbuzz.wat', () => {

  let wasmExports

  beforeEach(async () => {
    wasmExports = await wasm.instantiate(wasmFilePath)
  })

  afterEach(() => {
    wasmExports = undefined
  })

  it('should build and init', () => {
    assert.equal(1, 1)
  })

  it('should test fizz', () => {
    assert.equal(wasmExports.isFizz(1), 0, '1 should NOT be fizz')
    assert.equal(wasmExports.isFizz(3), 1, '3 should be fizz')
    assert.equal(wasmExports.isFizz(5), 0, '5 should NOT be fizz')
    assert.equal(wasmExports.isFizz(15), 1, '15 should be fizz')
  })

  it('should test buzz', () => {
    assert.equal(wasmExports.isBuzz(1), 0, '1 should NOT be buzz')
    assert.equal(wasmExports.isBuzz(3), 0, '3 should NOT be buzz')
    assert.equal(wasmExports.isBuzz(5), 1, '5 should be buzz')
    assert.equal(wasmExports.isBuzz(15), 1, '15 should NOT be buzz')
  })

  it('should test fizzbuzz', () => {
    assert.equal(wasmExports.isFizzBuzz(1), 0, '1 should NOT be fizzbuzz')
    assert.equal(wasmExports.isFizzBuzz(3), 0, '3 should NOT be fizzbuzz')
    assert.equal(wasmExports.isFizzBuzz(5), 0, '5 should NOT be fizzbuzz')
    assert.equal(wasmExports.isFizzBuzz(15), 1, '15 should be fizzbuzz')
  })

  it('should have defined memory', () => {
    assert.equal(!!wasmExports.memory, true, 'memory should exists')
    assert.equal(wasmExports.memory instanceof WebAssembly.Memory, true, 'memory should be an instance of WebAssembly.Memory')
    assert.equal(!!wasmExports.memory.buffer, true, 'memory should have buffer')
    assert.equal(wasmExports.memory.buffer instanceof ArrayBuffer, true, 'memory buffer should be an instance of ArrayBuffer')
  })

  it('should have empty memory at the start', () => {
    // check numbers from 1 to 100 (in fizzbuzz numbers stars from 1)
    const bytes = new Uint8Array(wasmExports.memory.buffer, 0, 100)
    bytes.forEach((byte) => {
      assert.equal(byte, 0, 'memory values should be 0')
    })
  })

  it('should fill memory with answers', () => {
    const numbersCount = 15
    wasmExports.compute(numbersCount)
    const elements = memoryParser(wasmExports.memory.buffer)

    assert.equal(elements.length, numbersCount, 'size of result should be defined in first 4 bytes')

    assert.equal(fizzBuzzMap[elements[0]]  , 'number'   , 'number 1 should be just number')
    assert.equal(fizzBuzzMap[elements[1]]  , 'number'   , 'number 2 should be just number')
    assert.equal(fizzBuzzMap[elements[2]]  , 'fizz'     , 'number 3 should be fizz')
    assert.equal(fizzBuzzMap[elements[3]]  , 'number'   , 'number 4 should be just number')
    assert.equal(fizzBuzzMap[elements[4]]  , 'buzz'     , 'number 5 should be buzz')
    assert.equal(fizzBuzzMap[elements[5]]  , 'fizz'     , 'number 6 should be fizz')
    assert.equal(fizzBuzzMap[elements[6]]  , 'number'   , 'number 7 should be just number')
    assert.equal(fizzBuzzMap[elements[7]]  , 'number'   , 'number 8 should be just number')
    assert.equal(fizzBuzzMap[elements[8]]  , 'fizz'     , 'number 9 should be fizz')
    assert.equal(fizzBuzzMap[elements[9]]  , 'buzz'     , 'number 10 should be buzz')
    assert.equal(fizzBuzzMap[elements[10]] , 'number'   , 'number 11 should be just number')
    assert.equal(fizzBuzzMap[elements[11]] , 'fizz'     , 'number 12 should be fizz')
    assert.equal(fizzBuzzMap[elements[12]] , 'number'   , 'number 13 should be just number')
    assert.equal(fizzBuzzMap[elements[13]] , 'number'   , 'number 14 should be just number')
    assert.equal(fizzBuzzMap[elements[14]] , 'fizzbuzz' , 'number 15 should be fizzbuzz')
  })

  it('should restrict max value to 65531 and min value to 0', () => {
    function testSize(given, expected) {
      if (typeof expected === 'undefined') expected = given
      wasmExports.compute(given)
      assert.equal(memoryParser(wasmExports.memory.buffer).length, expected, `Size should be: ${expected}`)
    }

    testSize(10)
    testSize(100)
    testSize(5)
    testSize(65531)
    testSize(65531)
    testSize(66531, 65531)
    testSize(0)
    testSize(-1, 0)
    testSize(-10, 0)
  })

})
