import fizzBuzzMap from './fizzbuzz-map.js'
import memoryParser from './memory-parser.js'

(async () => {

  const numbersCount = 100

  const file = await fetch('fizzbuzz.wasm')
  const wasm = await WebAssembly.instantiateStreaming(file)
  const { compute, memory } = wasm.instance.exports

  compute(numbersCount)

  const elements = memoryParser(memory.buffer)
  const elementOlist = document.createElement('ol')

  elements.forEach((n, i) => {
    const elementListItem = document.createElement('li')
    const result = fizzBuzzMap[n]

    elementListItem.textContent = result === 'number' ? i+1 : result
    elementOlist.appendChild(elementListItem)
  })

  document.body.appendChild(elementOlist)

})()
