export default function (memoryBuffer) {
  const size = new Uint32Array(memoryBuffer, 0, 1)[0]
  return new Uint8Array(memoryBuffer, 4, size)
}
