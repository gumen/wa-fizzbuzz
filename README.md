# FizzBuzz in WebAssembly

Written directly in WebAssembly text format.  Just because it's more fun.

## NVM (Node Version Manager)

For better experience [install and use nvm](https://github.com/nvm-sh/nvm#install--update-script) so you can easily switch to required NodeJS version.

Once nvm is installed execute this command in project root directory:

```sh
$ nvm use
```

## Build

```sh
$ npm i # install dependencies
$ npm t # run tests
```

## Usage

Run this command to start server with simple website that compute fizzbuzz for 100 numbers using fizzbuzz.wasm:

```sh
$ npm start
```

Then open http://localhost:8080
