(module
  (memory (export "memory") 1)

  (func $isFizz (export "isFizz") (param $number i32) (result i32)
    (i32.eq (i32.const 0) (i32.rem_u (get_local $number) (i32.const 3))))

  (func $isBuzz (export "isBuzz") (param $number i32) (result i32)
    (i32.eq (i32.const 0) (i32.rem_u (get_local $number) (i32.const 5))))

  (func $isFizzBuzz (export "isFizzBuzz") (param $number i32) (result i32)
    (i32.and
      (call $isFizz (get_local $number))
      (call $isBuzz (get_local $number))))

  (func (export "compute") (param $max i32)
    (local $loop_i i32)
    (local $memo_i i32)
    (set_local $loop_i (i32.const 1))
    (set_local $memo_i (i32.const 4))

    ;; $max value can't be lower then 0
    (if (i32.lt_s (get_local $max) (i32.const 0))
      (set_local $max (i32.const 0)))

    ;; $max value can't be higher then 65531
    (if (i32.gt_s (get_local $max) (i32.const 65531))
      (set_local $max (i32.const 65531)))

    ;; store $max value in first 4 memory bytes
    (i32.store (i32.const 0) (get_local $max))

    (block $end
      (loop $loop
        (block $continue
          (br_if $end (i32.gt_u (get_local $loop_i) (get_local $max)))

          (if (call $isFizzBuzz (get_local $loop_i))
            (then
              (i32.store8 (get_local $memo_i) (i32.const 3))
              (br $continue)))

          (if (call $isBuzz (get_local $loop_i))
            (then
              (i32.store8 (get_local $memo_i) (i32.const 2))
              (br $continue)))

          (if (call $isFizz (get_local $loop_i))
            (then
              (i32.store8 (get_local $memo_i) (i32.const 1))
              (br $continue)))

          ;; number
          (i32.store8 (get_local $memo_i) (i32.const 0))
          )

        ;; $loop_i++ and $memo_i++
        (set_local $loop_i (i32.add (get_local $loop_i) (i32.const 1)))
        (set_local $memo_i (i32.add (get_local $memo_i) (i32.const 1)))

        (br $loop))))
  )
