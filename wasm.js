import { promises as fs } from 'fs'

export const instantiate = async (inputWasmFilePath, imports={}) => {
  try {
    const file     = await fs.readFile(inputWasmFilePath)
    const module   = await WebAssembly.compile(file)
    const instance = await WebAssembly.instantiate(module, imports)

    return instance.exports
  } catch (error) {
    console.error('wasm.js | instantiate | It\'s a trap!\n', error)
  }
}

export default { instantiate }
